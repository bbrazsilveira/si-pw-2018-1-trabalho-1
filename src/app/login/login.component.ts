import { ApiService } from './../api.service';
import { Component, OnInit, ViewEncapsulation, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

declare var Materialize;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

    firstAttempt: boolean = true;
    connecting: boolean = false;
    nickname: string = "";
    api: ApiService;
    router: Router;

    constructor(api: ApiService, router: Router) {
        this.router = router;
        this.api = api;

        if (api.isLogged()) {
            this.router.navigateByUrl('/chat');
        }
    }

    ngOnInit() {
    }

    login() {
        if (this.nickname.length > 0) {
            this.connecting = true;
            this.api.login(this.nickname);
        }
        this.firstAttempt = false;
    }
}
