import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ApiService } from './api.service';
import { FormsModule } from '@angular/forms';
import { DialogComponent } from './dialog/dialog.component';
import { MaterializeModule } from 'angular2-materialize';
import { ChatComponent } from './chat/chat.component';

const appRoutes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: '#', redirectTo: '', pathMatch: 'full' },
    { path: 'login/#', redirectTo: '', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'chat', component: ChatComponent },
    { path: '**', redirectTo: '/login', pathMatch: 'full' }
];

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        DialogComponent,
        ChatComponent
    ],
    imports: [
        BrowserModule,
        MaterializeModule,
        FormsModule,
        RouterModule.forRoot(appRoutes)
    ],
    providers: [ApiService],
    bootstrap: [AppComponent]
})
export class AppModule { }
