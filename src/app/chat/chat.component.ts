import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class ChatComponent implements OnInit {

    api: ApiService;
    message: string = "";

    constructor(api: ApiService, private router: Router) {
        this.api = api;
        if (!api.isLogged()) {
            this.router.navigateByUrl('/login');
        }
    }

    ngOnInit() {}

    sendMessage() { 
        if (this.message.length > 0) {
            this.api.sendMessage(this.message);
            this.message = "";
        }
    }

    setSection(section: string) {
        this.message = "";
        this.api.setSection(section);
    }

}
